package com.example.freddy.practica2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class actividad2 extends AppCompatActivity {

    Button botonlogin, botoregistrar, botonbuscar,botonparametro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad2);
        botonlogin = (Button) findViewById(R.id.btnLogin);
        botoregistrar = (Button) findViewById(R.id.btnregistrar);
        botonbuscar = (Button) findViewById(R.id.btnbuscar);
        botonparametro = (Button) findViewById(R.id.btnpasarparametro);


        botonparametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(actividad2.this, actividapasarparametro.class);
                startActivity(intent);
            }
        });
        botonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(actividad2.this,actividapasarparametro.class);
                startActivity(intent);
            }
        });
    }
}
